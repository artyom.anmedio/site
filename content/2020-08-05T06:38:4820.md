+++
 title = "Tele2 Solutions Days"
 date = 2020-08-05
 description = "Tele2 Solutions Days"
+++

Tele2 Solutions Days

Место проведения: Online hackathon
Даты проведения: 21-23 августа
Подробная информация: https://vk.cc/axQbcW
Призовой фонд: 500 000 рублей

Tele2 совместно с «Лигой Цифровой Экономики» и e-Legion проводит хакатон Tele2 Solutions Days. Мероприятие проходит с целью поиска современных неожиданных технологических решений; обмена опытом с рынком. Организаторы предлагают участникам взглянуть на привычные вещи под новым углом.

К участию приглашаются разработчики (frontend и backend), дизайнеры, UI/UX-дизайнеры, Product Owner/Product Manager, а также все заинтересованные в современных технологиях.

У всех участников будет возможность пообщаться с ведущими разработчиками из компаний организаторов, узнать о корпоративной культуре Tele2 и о том, как это – работать в команде оператора связи по другим правилам. С участниками хакатона на протяжении всего мероприятия будут работать эксперты.

Участников ждут специальные номинации:
1. AR для розницы/программы лояльности
2. Разработка альтернативной версии маркета Tele2
3. Редизайн тариф-конструктора
4. Виджеты iOS 14 и приложения для часов
5. Идеи и прототипы многопользовательской игры

Отборочный тур продлится до 19 августа, а финал хакатона пройдет 21-23 августа. Для прохождения отборочного тура нужно заполнить анкету участника. В финал выйдут 50 команд. Общий призовой фонд составит 500 000 рублей, некоторых участников пригласят на работу в Tele2 и «Лигу Цифровой экономики». Но главный приз – возможность быстро, ярко и эффектно заявить о себе в профессиональном сообществе.

❗Количество мест ограничено, успейте подать заявку: https://vk.cc/axQbcW

#мероприятия