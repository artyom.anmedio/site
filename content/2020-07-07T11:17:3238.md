+++
 title = "Онлайн Митап Сегодня"
 date = 2020-07-07
 description = "Онлайн Митап Сегодня"
+++

Онлайн Митап Сегодня

В 19:30 МСК пройдёт NX QA Meetup #13

Михаил Сидельников и Василий Петухов выступят с докладом «Allure Server – история одного внедрения», а Егор Колдов в докладе «Используем HTTP моки в автотестах» расскажет, как можно тестировать внешние взаимодействия изолированно. 

Ждём вас! 

Для участия нужно зарегистрироваться – https://billing.timepad.ru/event/1347489/

#мероприятия